import groovy.transform.Field

@Field computersName = []

pipeline {
    agent none
    options {
        timestamps()
        timeout(time: 2, unit: 'HOURS')
        disableConcurrentBuilds()
    }
    parameters {
        choice(
            name: 'diskUsage',
            choices: '10\n90\n80\n70', // TODO: use '90\n80\n70'
            description: 'Max percent of disk usage threshold'
        )
    }
    stages {
        stage('Check size') {
            steps{
                script {
                    computersName = Jenkins.instance.computers.findAll { computer ->
                        echo "[${computer.displayName}].offline is ${computer.offline}"
                        return !computer.offline
                    }.findAll { computer ->
                        def node = computer.getNode()
                        def root = node.getRootPath()
                        def total = root.getTotalDiskSpace()
                        def free = root.getUsableDiskSpace()

                        def diskUsage = free / total * 100
                        echo "[${computer.displayName}]'s diskusage is ${diskUsage}%"

                        def maxPercentage = params.diskUsage.toInteger()

                        return diskUsage > maxPercentage
                    }.collect { computer ->
                        return computer.name
                    }
                }
            }
        }

        stage('Shutdowns all computers') {
            when {
                expression { -> computersName.size() > 0 }
            }
            steps{
                script {
                    computersName.collect { name ->
                        Jenkins.instance.getComputer(name)
                    }.each { computer ->
                        def message = "[${computer.displayName}] is offline temporarily before cleaning workspace"
                        echo message
                        slackSend channel: '#09_jenkins', color: 'warning', message: message
                        computer.setTemporarilyOffline(true)
                    }
                }
            }
        }

        stage('Wait running builds tp finish') {
            when {
                expression { -> computersName.size() > 0 }
            }
            steps{
                script {
                    computersName.collect { name ->
                        Jenkins.instance.getComputer(name)
                    }.each{ computer ->
                        waitUntil {
                            echo "There are ${computer.countBusy()}/${computer.countExecutors()} jobs are still running " +
                                 "in the computer [${computer.displayName}]"
                            return (computer.countBusy() == 0);
                        }
                    }
                }
            }
        }

        stage('Empty workspace') {
            when {
                expression { -> computersName.size() > 0 }
            }
            steps{
                script {
                    computersName.collect { name ->
                        Jenkins.instance.getComputer(name)
                    }.each{ computer ->
                        def node = computer.getNode()
                        def root = node.getRootPath()

                        def workspace = root.child("workspace")

                        // String message = "Start to delete workspace [${workspace}] on computer [${computer.displayName}]"
                        // echo message
                        // slackSend channel: '#09_jenkins', color: 'warning', message: message

                        try {

                            echo "111"
                            // echo "${computer.displayName}"
                            echo "222"
                            echo "${workspace.getTotalDiskSpace()}"
                            echo "333"
                            echo "${workspace.getUsableDiskSpace()}"

                            echo "444"
                            workspace.deleteContents()
                            echo "555"
                        } catch (Exception e) {
                            echo "Failed to clean disk of [${computer.displayName}]. error: [${e}]"
                            throw e
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            script {
                computersName.collect { name ->
                    Jenkins.instance.getComputer(name)
                }.each{ computer ->
                    computer.setTemporarilyOffline(false)
                    def message = "[${computer.displayName}] is online"
                    echo message
                    slackSend channel: '#09_jenkins', color: 'good', message: message
                }
            }
        }
    }
}